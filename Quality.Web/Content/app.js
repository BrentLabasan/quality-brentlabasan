console.log("Hello world");
var ReportController = (function () {
    function ReportController($scope) {
        this.$scope = $scope;
        $scope.helloWorld = "Hello World, from Angular!";
    }
    return ReportController;
}());
ReportController.$inject = ["$scope"];
angular.module("Report", [])
    .controller("ReportController", ReportController);
//# sourceMappingURL=app.js.map